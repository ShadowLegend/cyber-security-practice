FROM alpine:3.10

RUN apk update --no-cache &&\
	apk upgrade --no-cache &&\
	apk add vim build-base nasm bash gdb nmap nmap-scripts afl git python ftp &&\
	git clone https://github.com/longld/peda.git ~/peda &&\
	echo "source ~/peda/peda.py" >> ~/.gdbinit

CMD ["bash"]
