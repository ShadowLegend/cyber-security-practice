
nmap_scan := @sudo nmap -sV -T5 -O -Pn -A -D 10.0.0.1,10.0.0.2,10.0.0.3 -oG nmap-scan-result ${HOST}

default:
	@echo "Usage: <ENV_KEY0=VALUE0 ENV_KEY1=VALUE1...> make <target>\n"
	@echo "Targets:"
	@echo "	scan.port.basic: HOST"
	@echo "	scan.port.basic.specific: HOST,PORTS"
	@echo "	scan.port.advanced: HOST"
	@echo "	scan.port.advanced.specific: HOST,PORTS"
	@echo "	compile.afl.gcc: C_SOURCE"
	@echo "	analyze.afl: TEST_CASE_DIR,AFL_ANALYZR_RESULT_DIR\n"
	@echo "Example usage 0: HOST=scanme.nmap.org PORTS=22,80,173 make scan.port.basic.specific"
	@echo "Example usage 1: source .configured-env && make scan.port.basic.specific"

scan.port:
	$(nmap_scan) -p-

scan.port.specific:
	$(nmap_scan) -p ${PORTS}

scan.vulns:
	sudo proxychains nikto -h ${HOSTS}

scan.vulns.weaponized:
	sudo proxychains nikto -h -Format msf+ ${HOSTS}

compile.afl.gcc:
	afl-gcc -fno-stack-protector -z execstack ${C_SOURCE} -o afl-compiled

analyze.afl:
	afl-fuzz -i ${TEST_CASE_DIR} -o ${AFL_ANALYZE_RESULT_DIR} afl-compiled

gpg.verify:
	gpg --verify ${SIGNATURE} ${DATA}

